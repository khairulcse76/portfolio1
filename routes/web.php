<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/frontEnd.home');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::post('admin/user/insert', 'HomeController@user_insert');
Route::get('/admin', 'AdminController@login');

//.........Menu Bar Routes // ..........
Route::get('/admin/menubar', 'ManubarController@index');
Route::post('/admin/menubar/insert', 'ManubarController@insert');
Route::get('/admin/menubar/status-update-published/{id}', 'ManubarController@published');
Route::get('/admin/menubar/status-update-unpublished/{id}', 'ManubarController@unpublished');
Route::get('/admin/menubar/edit/{id}', 'ManubarController@edit');
Route::post('/admin/menubar/update', 'ManubarController@update');
Route::get('/admin/menubar/delete/{id}', 'ManubarController@delete');

//.........Menu Bar Routes // ..........
//.........Menu Bar Routes // ..........
//.........Menu Bar Routes // ..........
Route::get('/admin/portfolio', 'PortfolioController@index');
Route::post('/admin/portfolio-save', 'PortfolioController@portfolio_save');
Route::get('/admin/portfolio/edit/{id}', 'PortfolioController@edit');
Route::post('/admin/portfolio-update', 'PortfolioController@update');
Route::get('/admin/portfolio-delete/{id}', 'PortfolioController@delete');

//.........Menu Bar Routes // ..........
//.........Menu Bar Routes // ..........
//.........Menu Bar Routes // ..........
Route::get('/admin/massage', 'MassageController@index');
Route::get('/admin/msg-seen/{id}', 'MassageController@seen');
Route::get('/admin/msg-delete/{id}', 'MassageController@msg_delete');
Route::post('/message-insert', 'FrontEndController@message_insert');

//.........Menu Bar Routes // ..........
//.........Menu Bar Routes // ..........
//.........Menu Bar Routes // ..........
Route::get('/admin/users', 'ProfileController@index');
Route::post('/admin/user/insert', 'HomeController@user_insert');
Route::post('/admin/user/update', 'ProfileController@user_update');
Route::get('/admin/user/delete/{id}', 'ProfileController@user_delete');


//.........Users Profile // ..........
//.........Users Profile // ..........
//.........Users Profile // ..........
Route::get('/admin/user/profile', 'ProfileController@profile');