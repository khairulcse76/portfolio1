@extends('admin.layouts.admin-master')
@section('title') Tap Insert @endsection
@section('content-header')
    <li class="breadcrumb-item">
        <a href="{{ url('/home') }}">Dashboard</a>
    </li>
    <li class="breadcrumb-item active">Add New Tap</li>
@endsection
@section('main-content')
    <br><br>

    <div class="col-sm-6 ">
    <form method="post" action="{{ url('/admin/menubar/update/') }}" id="frm-tab-insert">
        @csrf
        <div class="form-group">
            <h2><label>Menu Tab Item Update</label></h2>
                <input type="text" name="tab_name" value="{{ $data->tab_name }}" placeholder="Enter new Tap" class="form-control">
                <input type="hidden" name="id" value="{{ $data->id }}">
                @if ($errors->has('tab_name'))
                    <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('tab_name') }}</strong>
                </span>
                @endif
            </div>
        <div class="form-group">
                <input type="submit" value="Update" class="form-control btn btn-primary">
            </div>
    </form>
    </div>

@endsection