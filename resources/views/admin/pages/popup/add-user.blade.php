<div class="modal fade" id="add-user" tabindex="-1" role="dialog" aria-labelledby="tap-insert" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3>Register A New User</h3>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><i class="fa fa-close"></i> </button>
            </div>
            <form action="{{ url('admin/user/insert') }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Name</label>
                        <div class="col-sm-5">
                            <input type="text" name="first_name" value="@if(old('first_name')) {{ old('first_name') }} @endif" class="form-control form-control-sm" id="smFormGroupInput" placeholder="First Name">
                            @if($errors->has('first_name'))
                                <small><span>{{ $errors->first('first_name') }}</span></small>
                            @endif
                        </div>
                        <div class="col-sm-5">
                            <input type="text" name="last_name" value="@if(old('last_name')) {{ old('last_name') }} @endif" class="form-control form-control-sm" id="smFormGroupInput" placeholder="Last Name">
                            @if($errors->has('last_name'))
                                <small><span>{{ $errors->first('last_name') }}</span></small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Email</label>
                        <div class="col-sm-10">
                            <input type="email" name="email" value="@if(old('email')) {{ old('email') }} @endif" class="form-control form-control-sm" id="smFormGroupInput" placeholder="example@gmail.com">
                            @if($errors->has('email'))
                                <small><span>{{ $errors->first('email') }}</span></small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Password</label>
                        <div class="col-sm-10">
                            <input type="password" name="password" value="@if(old('password')) {{ old('password') }} @endif" class="form-control form-control-sm" id="smFormGroupInput" placeholder="Enter Secure password">
                            @if($errors->has('password'))
                                <small><span>{{ $errors->first('password') }}</span></small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Confirm password</label>
                        <div class="col-sm-10">
                            <input type="password" name="password_confirmation"  class="form-control form-control-sm" id="smFormGroupInput" placeholder="Re-Enter your password">
                            @if($errors->has('password_confirmation'))
                                <small><span>{{ $errors->first('password_confirmation') }}</span></small>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary pull-right" data-dismiss="modal">Close</button>
                    <input type="submit" value="Save User" class="btn btn-success" type="button" >
                </div>
            </form>
        </div>
    </div>
</div>