<div class="modal fade" id="user-edit<?php echo $j++; ?>" tabindex="-1" role="dialog" aria-labelledby="tap-insert" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                From {{ $item->first_name }}
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><i class="fa fa-close"></i> </button>
            </div>
            <form action="{{ url('admin/user/update') }}" method="post">
                @csrf
                <input type="hidden" name="id" value="{{ $item->id }}">
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Name</label>
                        <div class="col-sm-5">
                            <input type="text" name="first_name" value="{{ $item->first_name }}" class="form-control form-control-sm" id="smFormGroupInput" placeholder="First Name">
                            @if($errors->has('first_name'))
                                <small><span>{{ $errors->first('first_name') }}</span></small>
                            @endif
                        </div>
                        <div class="col-sm-5">
                            <input type="text" name="last_name" value="{{  $item->last_name }}" class="form-control form-control-sm" id="smFormGroupInput" placeholder="Last Name">
                            @if($errors->has('last_name'))
                                <small><span>{{ $errors->first('last_name') }}</span></small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Email</label>
                        <div class="col-sm-10">
                            <input type="email" name="email" value="{{ $item->email }}" class="form-control form-control-sm" id="smFormGroupInput" placeholder="First Name">
                            @if($errors->has('email'))
                                <small><span>{{ $errors->first('email') }}</span></small>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    <input type="submit" value="update" class="btn btn-secondary" type="button" >
                </div>
            </form>
        </div>
    </div>
</div>