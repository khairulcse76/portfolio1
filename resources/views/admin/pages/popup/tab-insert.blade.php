<div class="modal fade" id="tap-insert" tabindex="-1" role="dialog" aria-labelledby="tap-insert" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Insert A New Item</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <form method="post" action="{{ url('/admin/menubar/insert') }}" id="frm-tab-insert">
                @csrf
            <div class="modal-body">
                <div class="form-group">
                    <input type="text" name="tab_name" placeholder="Enter new Tap" class="form-control">
                    @if ($errors->has('tab_name'))
                        <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('tab_name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <input type="submit" class="btn btn-primary">
            </div>
            </form>
        </div>
    </div>
</div>