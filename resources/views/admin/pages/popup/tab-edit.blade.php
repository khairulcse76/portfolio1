<div class="modal fade" id="tab-edit" tabindex="-1" role="dialog" aria-labelledby="tap-edit" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Item</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <form method="post" action="{{ url('/admin/menubar/update') }}" id="frm-tab-insert">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        @foreach($menuInfo as $menu)
                        <input type="text" name="tab_name" value="{{ $menu->tab_name }}" placeholder="Enter new Tap" class="form-control">
                          @endforeach
                            @if ($errors->has('tab_name'))
                            <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('tab_name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>