@extends('admin.layouts.admin-master')
@section('title') Manage Portfolio @endsection
@section('content-header')
    <li class="breadcrumb-item">
        <a href="{{ url('/home') }}">Dashboard</a>
    </li>
    <li class="breadcrumb-item active">
        <a href="{{ url('/home') }}">Manage Portfolio</a>
@endsection



@section('main-content')


    <span class="pull-right">
            @if ($errors->has('tab_name'))
            <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('tab_name') }}</strong>
                </span>
        @endif
    </span>
    @if(session('success'))
        <div class="form-group">
            <div class="col-sm-2"></div>
            <div class="col-sm-12">
                <div class="alert alert-success" style="font-size: large; padding: 2px; color: blue;"><center>{{ session('success') }}</center></div>
            </div>
        </div><hr>
    @endif
    @if(session('warning'))
        <div class="form-group">
            <div class="col-sm-2"></div>
            <div class="col-sm-12">
                <div class="alert alert-warning" style="font-size: large; padding: 2px; color: blue;"><center>{{ session('warning') }}</center></div>
            </div>
        </div><hr>
    @endif




    {{--@include('admin.pages.popup.tab-insert')--}}
    {{--@include('admin.pages.popup.tab-edit')--}}


    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Project  Managment
            <span class="pull-right" title="Add New Tap">
                <a href="{{ url('admin/portfolio-insert') }}" ><i class="fa fa-plus">ONE</i></a>
            </span>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Image</th>
                        <th>Project Name</th>
                        <th>Project Description</th>
                        <th colspan="">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $id=1; ?>
                    @foreach($portfolioInfo as $item)
                        <tr>
                            <td><?php echo $id++?></td>
                            <td>
                                <img src="{{ asset('upload/thumbs/'.$item->project_file) }}" class="img img-circle" width="100" height="100">
                            </td>
                            <td>
                                <input type="button" value="{{ $item->project_name }}" class="btn btn-success form-control">
                            </td>
                            <td class="details">
                                {{ $item->project_description }}
                            </td>
                            <td>
                                <a href="/admin/portfolio/edit/{{ $item->id }}"><i class="fa fa-edit"> Change </i></a> ||
                                <a href="/admin/portfolio-delete/{{ $item->id }}"><i class="fa fa-trash" onclick="return check_delete()"> Delete </i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
    </div>
@endsection
