@extends('admin.layouts.admin-master')
@section('title') Portfolio Insert @endsection
@section('content-header')
    <li class="breadcrumb-item">
        <a href="{{ url('/home') }}">Dashboard</a>
    </li>
    <li class="breadcrumb-item active">Insert New Item</li>
@endsection


@section('main-content')
    <span class="pull-right">
            @if ($errors->has('tab_name'))
            <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('tab_name') }}</strong>
                </span>
        @endif
    </span>
    @if(session('success'))
        <div class="form-group">
            <div class="col-sm-2"></div>
            <div class="col-sm-12">
                <div class="alert alert-success" style="font-size: large; padding: 2px; color: blue;"><center>{{ session('success') }}</center></div>
            </div>
        </div><hr>
    @endif
    @if(session('warning'))
        <div class="form-group">
            <div class="col-sm-2"></div>
            <div class="col-sm-12">
                <div class="alert alert-warning" style="font-size: large; padding: 2px; color: blue;"><center>{{ session('warning') }}</center></div>
            </div>
        </div><hr>
    @endif



    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Insert New Item
            <span class="pull-right" title="Add New Tap">
                <a href="{{ url('admin/portfolio') }}" ><i class="fa fa-fast-backward"> Back</i></a>
            </span>
        </div>
        <div class="card-body">
            <form method="post" action="{{ url('admin/portfolio-save') }}" id="frm-fortfolio-insert" enctype="multipart/form-data">
                @csrf
                <div class="form-group row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="project_name" value="{{ old('project_name') }}" id="staticEmail" placeholder="enter project name">
                        @if($errors->has('project_name'))
                            <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('project_name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="project_description" value="{{ old('project_description') }}" id="inputPassword" placeholder="Describe about your project">
                        @if($errors->has('project_description'))
                            <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('project_description') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <input type="file" class="form-control" name="project_file" id="projectFile" title="select Project Screen Short">
                        @if($errors->has('project_file'))
                            <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('project_file') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <input type="url" class="form-control" name="project_url" id="projectUrl" placeholder="Enter your project link">
                        @if($errors->has('project_url'))
                            <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('project_url') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-4">
                        <a href="{{ url('admin/portfolio') }}" type="button" class="form-control btn btn-default" title="Submit your Information" ><i class="fa fa-backward"> Back</i></a>
                    </div>
                    <div class="col-sm-4">
                        <input type="submit" class="form-control btn btn-primary" title="Submit your Information" >
                    </div>
                </div>
            </form>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
    </div>
@endsection

