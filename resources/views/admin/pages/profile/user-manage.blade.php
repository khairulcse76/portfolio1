@extends('admin.layouts.admin-master')
@section('title') User @endsection
@section('content-header')
    <li class="breadcrumb-item">
        <a href="{{ url('/home') }}">Dashboard</a>
    </li>
    <li class="breadcrumb-item active">
        <a href="{{ url('/admin/users') }}">User Manage</a>
        @endsection


        @section('main-content')
            @if ( count( $errors ) > 0 )
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>
                    @endforeach
                </div>
            @endif

            @include('admin.pages.popup.add-user')

            <span class="pull-right">
            @if ($errors->has('tab_name'))
                    <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('tab_name') }}</strong>
                </span>
                @endif
            </span>
            @if(session('success'))
                <div class="form-group">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-12">
                        <div class="alert alert-success" style="font-size: large; padding: 2px; color: blue;"><center>{{ session('success') }}</center></div>
                    </div>
                </div><hr>
            @endif
            @if(session('warning'))
                <div class="form-group">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-12">
                        <div class="alert alert-warning" style="font-size: large; padding: 2px; color: blue;"><center>{{ session('warning') }}</center></div>
                    </div>
                </div><hr>
            @endif

            <div class="card mb-3">
                <div class="card-header">
                    <i class="fa fa-table"></i> Users
                    <span class="pull-right" title="Add New Tap">
                <a href="" data-toggle="modal" data-target="#add-user" ><i class="fa fa-plus"></i></a>
            </span>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th colspan="3">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $id=1;
                            $i=1;
                            $j=1;
                            ?>
                            @foreach($users as $item)
                                <tr>
                                    <td><?php echo $id++?></td>
                                    <td>
                                        {{ $item->first_name." ".$item->last_name }}
                                    </td>
                                    <td>
                                        {{ $item->email }}
                                    </td>
                                    <td>
                                        Admin
                                    </td>
                                    <td class="details">
                                        <span class="pull-right" title="Add New Tap">
                                            <a href="{{ url('admin/user/profile') }}"><i class="fa fa-street-view">View</i></a>
                                        </span>
                                    </td>
                                    <td>
                                        <a href="/admin/user-edit/{{ $item->id }}" data-toggle="modal" data-target="#user-edit<?php echo $i++; ?>"><i class="fa fa-edit">Edit</i></a>
                                    <td>
                                        <a href="/admin/user/delete/{{ $item->id }}"><i class="fa fa-trash" onclick="return check_delete()"> Delete </i></a>
                                    </td>
                                </tr>


                                @include('admin.pages.popup.user-update')



                                <div class="modal fade" id="user-edit<?php echo $j++; ?>" tabindex="-1" role="dialog" aria-labelledby="tap-insert" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                From {{ $item->first_name }}
                                                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><i class="fa fa-close"></i> </button>
                                            </div>
                                        <form action="{{ url('admin/user/update') }}" method="post">
                                            @csrf
                                            <input type="hidden" name="id" value="{{ $item->id }}">
                                            <div class="modal-body">
                                                    <div class="form-group row">
                                                        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Name</label>
                                                        <div class="col-sm-5">
                                                            <input type="text" name="first_name" value="{{ $item->first_name }}" class="form-control form-control-sm" id="smFormGroupInput" placeholder="First Name">
                                                            @if($errors->has('first_name'))
                                                                <small><span>{{ $errors->first('first_name') }}</span></small>
                                                            @endif
                                                        </div>
                                                        <div class="col-sm-5">
                                                            <input type="text" name="last_name" value="{{  $item->last_name }}" class="form-control form-control-sm" id="smFormGroupInput" placeholder="Last Name">
                                                            @if($errors->has('last_name'))
                                                                <small><span>{{ $errors->first('last_name') }}</span></small>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Email</label>
                                                        <div class="col-sm-10">
                                                            <input type="email" name="email" value="{{ $item->email }}" class="form-control form-control-sm" id="smFormGroupInput" placeholder="First Name">
                                                            @if($errors->has('email'))
                                                                <small><span>{{ $errors->first('email') }}</span></small>
                                                            @endif
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                                                <input type="submit" value="update" class="btn btn-secondary" type="button" >
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>


                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
            </div>
@endsection
