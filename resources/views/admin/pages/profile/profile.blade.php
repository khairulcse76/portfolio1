@extends('admin.layouts.admin-master')
@section('title') User|Profile @endsection
@section('content-header')
    <li class="breadcrumb-item">
        <a href="{{ url('/home') }}">Dashboard</a>
    </li>
    <li class="breadcrumb-item active">
        <a href="{{ url('/admin/user/profile') }}">User Profile</a>
        @endsection


        @section('main-content')
            @if ( count( $errors ) > 0 )
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br>
                    @endforeach
                </div>
            @endif

            @include('admin.pages.popup.add-user')
            @include('admin.pages.css.style')

            <span class="pull-right">
            @if ($errors->has('tab_name'))
                    <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('tab_name') }}</strong>
                </span>
                @endif
            </span>
            @if(session('success'))
                <div class="form-group">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-12">
                        <div class="alert alert-success" style="font-size: large; padding: 2px; color: blue;"><center>{{ session('success') }}</center></div>
                    </div>
                </div><hr>
            @endif
            @if(session('warning'))
                <div class="form-group">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-12">
                        <div class="alert alert-warning" style="font-size: large; padding: 2px; color: blue;"><center>{{ session('warning') }}</center></div>
                    </div>
                </div><hr>
            @endif

                <div class="row">
                    <div class="col-lg-2 col-sm-2"></div>
                    <div class="col-lg-8 col-sm-8">
                        <div class="card hovercard">
                            <div class="cardheader">
                            </div>
                            <div class="avatar">
                                <img alt="" src="https://avatars1.githubusercontent.com/u/11767240?v=3&s=400">
                            </div>
                            <div class="info">
                                <div class="title">
                                    <a target="_blank" href="">Kalyanasundaram</a>
                                </div>
                                <div class="desc">Backend Developer, ML</div>
                                <div class="desc">Developer at <b>Freshdesk</b></div>
                                <div class="">Developer at <b>Freshdesk</b></div>
                            </div>
                        </div>

                    </div>

                </div>
@endsection
