@extends('admin.layouts.admin-master')
@section('title') User||Massages @endsection
@section('content-header')
    <li class="breadcrumb-item">
        <a href="{{ url('/home') }}">Dashboard</a>
    </li>
    <li class="breadcrumb-item active">
        <a href="{{ url('/admin/massage') }}">Manage Massage</a>
        @endsection



        @section('main-content')


            <span class="pull-right">
            @if ($errors->has('tab_name'))
                    <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('tab_name') }}</strong>
                </span>
                @endif
    </span>
            @if(session('success'))
                <div class="form-group">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-12">
                        <div class="alert alert-success" style="font-size: large; padding: 2px; color: blue;"><center>{{ session('success') }}</center></div>
                    </div>
                </div><hr>
            @endif
            @if(session('warning'))
                <div class="form-group">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-12">
                        <div class="alert alert-warning" style="font-size: large; padding: 2px; color: blue;"><center>{{ session('warning') }}</center></div>
                    </div>
                </div><hr>
            @endif




            {{--@include('admin.pages.popup.tab-insert')--}}
            {{--@include('admin.pages.popup.tab-edit')--}}


            <div class="card mb-3">
                <div class="card-header">
                    <i class="fa fa-table"></i> Massage
                    <span class="pull-right" title="Add New Tap">
                <a href="" ><i class="fa fa-plus"></i></a>
            </span>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Message</th>
                                <th colspan="">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $id=1;
                            $i=1;
                            $j=1;

                            ?>
                            @foreach($massage as $item)
                                <tr>
                                    <td><?php echo $id++?></td>
                                    <td>
                                        {{ $item->name }}
                                    </td>
                                    <td>
                                        {{ $item->email }}
                                    </td>
                                    <td>
                                        {{ $item->phone }}
                                    </td>
                                    <td class="details">
                                        <span class="pull-right" title="Add New Tap">
                                            <a href="" data-toggle="modal" data-target="#msg-view<?php echo $i++; ?>"><i class="fa fa-street-view">View</i></a>
                                        </span>
                                    </td>
                                    <td>
                                        <a href="/admin/msg-delete/{{ $item->id }}"><i class="fa fa-trash" onclick="return check_delete()"> Delete </i></a>
                                    </td>
                                </tr>

                                <div class="modal fade" id="msg-view<?php echo $j++; ?>" tabindex="-1" role="dialog" aria-labelledby="tap-insert" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                               From {{ $item->name }}
                                                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><i class="fa fa-close"></i> </button>

                                            </div>
                                            <div class="modal-body">
                                                {{ $item->message }}
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
            </div>
@endsection
