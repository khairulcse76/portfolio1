@extends('admin.layouts.admin-master')
@section('title') Manage Menu @endsection
@section('content-header')
    <li class="breadcrumb-item">
        <a href="{{ url('/home') }}">Dashboard</a>
    </li>
    <li class="breadcrumb-item active">Manage Menu</li>
@endsection
@section('main-content')
    <span class="pull-right">
            @if ($errors->has('tab_name'))
            <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('tab_name') }}</strong>
                </span>
        @endif
    </span>
    @if(session('success'))
        <div class="form-group">
            <div class="col-sm-2"></div>
            <div class="col-sm-12">
                <div class="alert alert-success" style="font-size: large; padding: 2px; color: blue;"><center>{{ session('success') }}</center></div>
            </div>
        </div><hr>
    @endif
    @if(session('warning'))
        <div class="form-group">
            <div class="col-sm-2"></div>
            <div class="col-sm-12">
                <div class="alert alert-warning" style="font-size: large; padding: 2px; color: blue;"><center>{{ session('warning') }}</center></div>
            </div>
        </div><hr>
    @endif




    @include('admin.pages.popup.tab-insert')
    @include('admin.pages.popup.tab-edit')


    <div class="card mb-3">
        <div class="card-header">
            <i class="fa fa-table"></i> Menubar Managment
            <span class="pull-right" title="Add New Tap">
                <a href="" data-toggle="modal" data-target="#tap-insert"><i class="fa fa-plus">ONE</i></a>
            </span>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Tab Name</th>
                        <th>Status</th>
                        <th colspan="">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $id=1; ?>
                    @foreach($menuInfo as $menu)
                    <tr>
                        <td><?php echo $id++?></td>
                        <td>
                            <input type="button" value="{{ $menu->tab_name }}" class="btn btn-success form-control">
                        </td>
                        <td>
                            @if($menu->status == 0)
                                <a href="/admin/menubar/status-update-published/{{ $menu->id }}" type="button" value="" class="btn btn-warning form-control">Unpublished</a>
                                @else
                                <a href="/admin/menubar/status-update-unpublished/{{ $menu->id }}" type="button" value="" class="btn btn-primary form-control">Published</a>
                                @endif
                        </td>
                        <td>
                            <a href="/admin/menubar/edit/{{ $menu->id }}"><i class="fa fa-edit"> Change </i></a> ||
                            <a href="/admin/menubar/delete/{{ $menu->id }}"><i class="fa fa-trash" onclick="return check_delete()"> Delete </i></a>
                        </td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
    </div>
@endsection
