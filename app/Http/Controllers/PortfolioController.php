<?php

namespace App\Http\Controllers;

use App\Portfolio;
use Illuminate\Http\Request;
use Image;
use DB;
class PortfolioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $portfolioInfo=Portfolio::all();
        return view('admin.pages.portfolio.portfolio-manage', compact('portfolioInfo', $portfolioInfo));
    }

    public function portfolio_insert(){

        return view('admin.pages.portfolio.portfolio-insert');
    }

    public function portfolio_save(Request $request){
        print_r($request->all());
        $this->validate($request,[
            'project_name' => 'required|min:4|max:50',
            'project_description' => 'required|min:3|max:200',
//            'project_file' => 'required|min:3|max:20',
        ]);

        $data=new Portfolio;
        $data->project_name = $request->project_name;
        $data->user_id = auth()->user()->id;
        $data->project_description = $request->project_description;
        $data->project_file = $request->project_file;
        $data->project_url = $request->project_url;


        $image=$request->file('project_file');
        if ($image){
            $this->validate($request,[
                'project_file' => 'required|image|mimes:jpeg,jpg,png,gif,svg|max:500',
            ]);
            $data['project_file'] = $this->imageUpload($image);
        }
        $data->save();
        session()->flash('success', 'Project Successfully Save....!');
        return redirect()->back();
    }
    public function update(Request $request){

        $this->validate($request,[
            'project_name' => 'required|min:4|max:50',
            'project_description' => 'required|min:3|max:200'
        ]);

        $data=Portfolio::find($request->id);
//        $data=Portfolio::where('id', $request->id);
        $data->project_name = $request->project_name;
        $data->project_description = $request->project_description;
        $data->project_url = $request->project_url;

        $image=$request->file('project_file');
        $oldImage=$data->project_file;

        if ($image){
            $this->validate($request,[
                'project_file' => 'required|image|mimes:jpeg,jpg,png,gif,svg|max:500',
            ]);
            $this->file_delete($oldImage);
            $data['project_file'] = $this->imageUpload($image);
        }else{
            $data->project_file=$oldImage;
        }

        $data->save();
        session()->flash('success', 'Project Successfully Update....!');
        return redirect('admin/portfolio');
    }


    public function edit($id){
        $editdata=Db::table('portfolios')->where('id', $id)->first();
        return view('admin.pages.portfolio.portfolio-edit')->with('data', $editdata);
    }
    public function delete($id){
        $deletedata=DB::table('portfolios')->where('id', $id)->first();

        $image=$deletedata->project_file;

        if ($image){
            $delete=$this->file_delete($image);
            Portfolio::findOrFail($id)->delete();
            session()->flash('warning', 'Project Permanently Delete..');
        }

        return redirect('admin/portfolio');
    }


    private function imageUpload($image){
        $originalName= $image->getClientOriginalName();
        $inpute=md5($originalName).time().'.'.$image->getClientOriginalExtension();
        $destinationPaththumbs=public_path('/upload/thumbs');
        $imgthumbs= Image::make($image->getRealPath());
        $imgthumbs->resize(100, 100, function ($constraint){
            $constraint->aspectRatio();
        })->save($destinationPaththumbs.'/'.$inpute);

        $destinationPathpicture=public_path('/upload/picture');
        $imgpicture= Image::make($image->getRealPath());
        $imgpicture->resize(450, 600, function ($constraint){
            $constraint->aspectRatio();
        })->save($destinationPathpicture.'/'.$inpute);

        $destinationPaththumbs2=public_path('/upload/thumbs2');
        $imgthumbs2= Image::make($image->getRealPath());
        $imgthumbs2->resize(300, 300, function ($constraint){
            $constraint->aspectRatio();
        })->save($destinationPaththumbs2.'/'.$inpute);

        $destinationPathUpload=public_path('/upload');
        $success=$image->move($destinationPathUpload, $inpute);

        if ($success){
            return $inpute;
        }else{
            die('Image Upload Fail');
        }
    }

    private function file_delete($file){
        \File::Delete('upload/'.$file, 'upload/thumbs/'.$file,'upload/thumbs2/'.$file, 'upload/picture/'.$file);
        $v=0;
        return $v;
    }
}
