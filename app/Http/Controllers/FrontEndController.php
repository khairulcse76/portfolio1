<?php

namespace App\Http\Controllers;

use App\Massage;
use App\Portfolio;
use Illuminate\Http\Request;

class FrontEndController extends Controller
{

    public function message_insert(Request $request){

        $this->validate($request, [
            'name'=>'required|min:2',
            'email'=>'required|email',
        ]);

        if ($request->phone){
            $this->validate($request, [
                'phone'=>'required|numeric|min:8',
            ]);
        }

        $data=new Massage;

        $data->name=$request->name;
        $data->email=$request->email;
        $data->phone=$request->phone;
        $data->message=$request->message;
        $data->save();
        session()->flash('success','Message send success');
        return back();
    }
}
