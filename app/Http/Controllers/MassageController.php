<?php

namespace App\Http\Controllers;

use App\Massage;
use Illuminate\Http\Request;

class MassageController extends Controller
{
    public function index(){
        $massage=Massage::OrderBy('id','desc')->get();
        return view('admin.pages.massage.massage-manage', compact('massage'));
    }
    public function seen($id){
        $massage=Massage::where('id',$id)->update(['status'=> 1]);
        return redirect('admin/massage');
    }
    public function msg_delete($id){
        $massage=Massage::findOrFail($id)->delete();
        session()->flash('warning','Massage permanently delete');
        return redirect('admin/massage');
    }
}
