<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $users=User::all();
        return view('admin.pages.profile.user-manage', compact('users'));
    }
    public function profile(){

        return view('admin.pages.profile.profile');
    }
    public function user_update(Request $request){
        $this->validate($request,[
            'first_name' => 'required|min:2',
            'last_name' => 'required|',
            'email' => 'required|email',
        ]);

        $data=User::where('id', $request->id)->update([
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'email'=>$request->email,
            'updated_at'=>Carbon::now(),
        ]);

        if ($data){
           session()->flash('success', 'User data update success');
        }
         return back();
    }
    public function user_delete($id){
        User::findOrFail($id)->delete();
        session()->flash('warning', 'User permanently delete');
        return back();
    }
}
