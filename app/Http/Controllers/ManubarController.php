<?php

namespace App\Http\Controllers;

use App\Manubar;
use Illuminate\Http\Request;

class ManubarController extends Controller
{
    public function index(){

        $menuInfo=Manubar::all();
        return view('admin.pages.manubar-manage', compact('menuInfo', $menuInfo));
    }
    public function insert(Request $request){


        $this->validate($request,[
            'tab_name' => 'required|min:3|max:20',
        ]);

        $data=new Manubar;
        $data->tab_name = $request->tab_name;
        $data->save();
        session()->flash('success', 'Menu Tab Successfully Insert....!');
        return redirect()->back();
    }
    public function published($id){
        $data=Manubar::where('id', $id)->update(['status'=>1]);
        session()->flash('success', 'Published....!');
        return back();
    }
    public function unpublished($id){
        $data=Manubar::where('id', $id)->update(['status'=>0]);
        session()->flash('warning', 'Unpublished....!');
        return back();
    }
    public function edit($id){
       $data=Manubar::findOrFail($id);
        return view('admin.pages.menubar.menubar-edit')->with('data', $data);
    }
    public function update(Request $request){
        Manubar::findOrFail($request->id)->update(['tab_name'=>$request->tab_name]);
        session()->flash('success', 'Navbar Tab successfully Update..');
        return redirect('admin/menubar');
    }
    public function delete($id){
        Manubar::findOrFail($id)->delete();
        session()->flash('warning', 'Navbar Tab successfully Delete..');
        return redirect('admin/menubar');
    }
}
