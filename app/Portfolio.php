<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    protected $fillable=['user_id','project_name','project_description','project_file','project_url',];
}
